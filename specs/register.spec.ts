import {test, expect, Page, Locator} from '@playwright/test';
import {chromium} from "@playwright/test";
import {LoginPage} from "../pages/login.page";
import {RegisterPage} from "../pages/register.page";
import { faker } from '@faker-js/faker';
import {RegisterInterface} from "../interfaces/register.interface";
import {invalidEmail, registerPassword} from "../constants/register.constants";


let page: Page;
let testName;

test.beforeAll(async ({ browser }) => {
    page = await browser.newPage();
    await page.goto('/demo');
});

test.afterAll(async () => {
    await page.close();
});

test.beforeEach(({}, testInfo)=>{
    testName = testInfo.title
})

test.describe.configure({mode: 'serial'})
test.describe('Login page', async() => {

    test('Register page should be displayed', async()=>{
        const loginPage = new LoginPage(page)
        const registerPage = new RegisterPage(page)
        await page.locator('a[title="My Account"]').click()
        await page.locator('a:has-text("Register")').click()
        await registerPage.waitForRegisterPageToBeDisplayed()
    })


    test('should show required errors', async () => {
        const registerPage = new RegisterPage(page)
        await registerPage.policyCheckbox.scrollIntoViewIfNeeded()
        await registerPage.policyCheckbox.check()
        await registerPage.registerBtn.click()
        await expect(registerPage.errorMsg).toHaveCount(5)
        await page.screenshot({path: `./screenshots/${testName}.png`, fullPage: true})
    })

    test ('should show error on firstName field with more than 32 characters', async ()=>{

        const registerBody: RegisterInterface ={
            firstName: faker.datatype.string(33),
            lastName: faker.name.firstName(),
            email: faker.internet.email(),
            phone: faker.phone.number(),
            password: registerPassword,
            confirmPassword: registerPassword
        }
        const registerPage = new RegisterPage(page)
        await registerPage.register(registerBody)
        await registerPage.registerBtn.click()
        await expect(registerPage.errorMsg.first()).toHaveText('First Name must be between 1 and 32 characters!')
    })

    test ('should show error on lastName field with more than 32 characters', async ()=>{

        const registerBody: RegisterInterface ={
            firstName: faker.name.firstName(),
            lastName: faker.datatype.string(33),
            email: faker.internet.email(),
            phone: faker.phone.number(),
            password: registerPassword,
            confirmPassword: registerPassword
        }

        const registerPage = new RegisterPage(page)
        await registerPage.register(registerBody)
        await registerPage.registerBtn.click()
        await expect(registerPage.errorMsg).toHaveText('Last Name must be between 1 and 32 characters!')
    })

    test ('should show error on email field with invalid email format', async ()=>{

        const registerBody: RegisterInterface ={
            firstName: faker.name.firstName(),
            lastName: faker.name.lastName(),
            email: invalidEmail,
            phone: faker.phone.number(),
            password: registerPassword,
            confirmPassword: registerPassword
        }

        const registerPage = new RegisterPage(page)
        await registerPage.register(registerBody)
        await registerPage.registerBtn.click()
        await expect(registerPage.errorMsg).toHaveText('E-Mail Address does not appear to be valid!')
    })

})
