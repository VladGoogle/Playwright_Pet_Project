import {Locator, Page} from "@playwright/test";

export class MainPage {
    readonly page: Page;
    readonly hotelsTab: Locator;
    readonly flightsTab: Locator;
    readonly toursTab: Locator;
    readonly carsTab: Locator;
    readonly visaTab: Locator;
    readonly cityDropdown: Locator;
    readonly cityDropdownResults: Locator;
    readonly checkinDatePicker: Locator;
    readonly checkoutDatePicker: Locator;
    readonly travellersDropdown: Locator;
    readonly hotelSubmitBtn: Locator;
    readonly toursSubmitBtn: Locator;
    readonly carsSubmitBtn: Locator;
    readonly roomDecBtn: Locator;
    readonly roomIncBtn: Locator;
    readonly roomsInput: Locator;
    readonly adultsBlock: Locator;
    readonly childBlock: Locator;
    readonly infantBlock: Locator;
    readonly adultsDecBtn: Locator;
    readonly adultsIncBtn: Locator;
    readonly adultsInput: Locator;
    readonly childDecBtn: Locator;
    readonly childIncBtn: Locator;
    readonly childInput: Locator;
    readonly nationalityDropdown: Locator;
    readonly roundTripRadio: Locator;
    readonly oneWayRadio: Locator;
    readonly flyingFromInput: Locator;
    readonly flyingToInput: Locator;
    readonly departureDatePicker: Locator;
    readonly infantDecBtn: Locator;
    readonly infantIncBtn: Locator;
    readonly flightTypeDropdown: Locator;
    readonly flightSearchBtn: Locator;
    readonly destinationDropdown: Locator;
    readonly toursDatePicker: Locator;
    readonly carFromDropdown: Locator;
    readonly carDestinationDropdown: Locator;
    readonly carFromDatePicker : Locator;
    readonly carToDatePicker : Locator;
    readonly visaFromDropdown: Locator;
    readonly visaToDropdown: Locator;
    readonly visaDatePicker: Locator;
    readonly visaSubmitBtn: Locator;
    constructor(page: Page) {
        this.page = page;
        this.hotelsTab = page.locator('#hotels-tab')
        this.flightsTab = page.locator('#flights-tab')
        this.toursTab = page.locator('#tours-tab')
        this.carsTab = page.locator('#cars-tab')
        this.visaTab = page.locator('#visa-tab')
        this.cityDropdown = page.locator('span[aria-labelledby="select2-hotels_city-container"]')
        this.cityDropdownResults = page.locator('#select2-hotels_city-results')
        this.checkinDatePicker = page.locator('#checkin')
        this.checkoutDatePicker = page.locator('#checkout')
        this.travellersDropdown = page.locator('xpath=/html/body/section[1]/div/div/div/div/div[2]/div[2]/div[4]/form/div/div/div[3]/div/div/div/a')
        this.hotelSubmitBtn = page.locator('#hotels-search #submit')
        this.toursSubmitBtn = page.locator('#tours-search #submit')
        this.carsSubmitBtn = page.locator('#cars-search #submit')
        this.roomDecBtn = page.locator('.roomDec')
        this.roomIncBtn = page.locator('.roomInc')
        this.roomsInput = page.locator('#rooms')
        this.adultsBlock = page.locator('xpath=//*[@id="hotels-search"]/div/div/div[3]/div/div/div/div/div[2]/div')
        this.adultsIncBtn = this.adultsBlock.locator('.qtyInc')
        this.adultsDecBtn = this.adultsBlock.locator('.qtyDec')
        this.adultsInput = page.locator('#adults')
        this.childBlock = page.locator('xpath=//*[@id="hotels-search"]/div/div/div[3]/div/div/div/div/div[3]/div')
        this.childIncBtn = this.childBlock.locator('.qtyInc')
        this.childDecBtn = this.childBlock.locator('.qtyDec')
        this.childInput = page.locator('#childs')
        this.nationalityDropdown = page.locator('#nationality')
        this.flightTypeDropdown = page.locator('#flight_type')
        this.roundTripRadio = page.locator('#round-trip')
        this.oneWayRadio = page.locator('#one-way')
        this.flyingFromInput = page.locator('#autocomplete')
        this.flyingToInput = page.locator('#autocomplete2')
        this.departureDatePicker = page.locator('#onereturn #departure')
        this.infantBlock = page.locator('div.infant_qty')
        this.infantIncBtn = this.infantBlock.locator('.qtyInc')
        this.infantDecBtn = this.infantBlock.locator('.qtyDec')
        this.flightSearchBtn = page.locator('#flights-search')
        this.destinationDropdown = page.locator('span[aria-labelledby="select2-tours_city-container"]')
        this.toursDatePicker = page.locator('#tours-search #date')
        this.carFromDropdown = page.locator('span[aria-labelledby="select2-carfrom-container"]')
        this.carDestinationDropdown = page.locator('span[aria-labelledby="select2-carto-container"]')
        this.carFromDatePicker = page.locator('#datefrom')
        this.carToDatePicker = page.locator('#dateto')
        this.visaFromDropdown = page.locator('span[aria-labelledby="select2-from_country-container"]')
        this.visaToDropdown = page.locator('span[aria-labelledby="select2-to_country-container"]')
        this.visaDatePicker = page.locator('#visa-submit #date')
        this.visaSubmitBtn = page.locator('#visa-submit #submit')
    }

    async waitForHotelsTabToBeDisplayed(): Promise<void> {
        await this.cityDropdown.isVisible()
        await this.checkinDatePicker.isVisible()
        await this.checkoutDatePicker.isVisible()
        await this.travellersDropdown.isVisible()
        await this.hotelSubmitBtn.isVisible()
    }

    async waitForFlightsTabToBeDisplayed(): Promise<void> {
        await this.flyingFromInput.isVisible()
        await this.flyingToInput.isVisible()
        await this.departureDatePicker.isVisible()
        await this.oneWayRadio.isVisible()
        await this.roundTripRadio.isVisible()
        await this.flightTypeDropdown.isVisible()
        await this.flightSearchBtn.isVisible()
    }


    async waitForToursTabToBeDisplayed(): Promise<void> {
        await this.destinationDropdown.isVisible()
        await this.toursDatePicker.isVisible()
        await this.travellersDropdown.isVisible()
        await this.toursSubmitBtn.isVisible()
    }

    async waitForTransfersTabToBeDisplayed(): Promise<void> {
        await this.carFromDropdown.isVisible()
        await this.carDestinationDropdown.isVisible()
        await this.carFromDatePicker.isVisible()
        await this.carToDatePicker.isVisible()
        await this.travellersDropdown.isVisible()
        await this.carsSubmitBtn.isVisible()
    }

    async waitForVisaTabToBeDisplayed(): Promise<void> {
        await this.visaFromDropdown.isVisible()
        await this.visaToDropdown.isVisible()
        await this.visaDatePicker.isVisible()
        await this.visaSubmitBtn.isVisible()
    }
}