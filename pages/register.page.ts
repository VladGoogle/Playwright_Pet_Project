import { expect, Locator, Page } from '@playwright/test';
import {RegisterInterface} from "../interfaces/register.interface";

export class RegisterPage {
    readonly page: Page;
    readonly firstNameInput: Locator;
    readonly lastNameInput: Locator;
    readonly emailInput: Locator;
    readonly phoneInput: Locator;
    readonly passwordInput: Locator;
    readonly confirmPasswordInput: Locator;
    readonly policyCheckbox: Locator;
    readonly loginLink: Locator;
    readonly registerBtn: Locator;
    readonly errorMsg: Locator;

    constructor(page: Page) {
        this.page = page;
        this.firstNameInput = page.locator('#input-firstname')
        this.lastNameInput = page.locator('#input-lastname')
        this.emailInput = page.locator('#input-email')
        this.phoneInput = page.locator('#input-telephone')
        this.passwordInput = page.locator('#input-password')
        this.confirmPasswordInput = page.locator('#input-confirm')
        this.policyCheckbox = page.locator('[name="agree"]')
        this.registerBtn = page.getByRole('button', { name: 'Continue' })
        this.loginLink = page.getByRole('link', { name: 'login page' })
        this.errorMsg = page.locator('.text-danger')
    }


    async waitForRegisterPageToBeDisplayed(): Promise<void> {
        await expect(this.firstNameInput).toBeVisible()
        await expect(this.lastNameInput).toBeVisible()
        await expect(this.emailInput).toBeVisible()
        await expect(this.passwordInput).toBeVisible()
        await expect(this.confirmPasswordInput).toBeVisible()
        await expect(this.phoneInput).toBeVisible()
        await expect(this.registerBtn).toBeVisible()
        await expect(this.loginLink).toBeVisible()
        await expect(this.policyCheckbox).toBeVisible()
    }
    async register(body: RegisterInterface): Promise<void> {
        await this.firstNameInput.fill(body.firstName)
        await this.lastNameInput.fill(body.lastName)
        await this.emailInput.fill(body.email)
        await this.phoneInput.fill(body.phone)
        await this.passwordInput.fill(body.password)
        await this.confirmPasswordInput.fill(body.confirmPassword)
    }
}