// playwright-dev-page.ts
import { expect, Locator, Page } from '@playwright/test';

export class LoginPage {
    readonly page: Page;
    readonly emailInput: Locator;
    readonly passwordInput: Locator;
    readonly rememberCheckbox: Locator;
    readonly loginBtn: Locator;
    readonly registerLink: Locator;

    constructor(page: Page) {
        this.page = page;
        this.emailInput = page.locator('#username')
        this.passwordInput = page.locator('#password')
        this.rememberCheckbox = page.locator('[name="remember"]')
        this.loginBtn = page.locator('button:has-text("Sign In")')
        this.registerLink = page.locator('[data-test="signup"]')
    }


    async waitForLoginPageToBeDisplayed(): Promise<void> {
        await expect(this.emailInput).toBeVisible()
        await expect(this.passwordInput).toBeVisible()
        await expect(this.rememberCheckbox).toBeVisible()
        await expect(this.loginBtn).toBeVisible()
        await expect(this.registerLink).toBeVisible()
    }
    async login(email: string, password: string): Promise<void> {
        await this.emailInput.fill(email)
        await this.passwordInput.fill(password)
        await this.loginBtn.click()
    }
}