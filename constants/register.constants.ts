import { faker } from '@faker-js/faker';

export const registerPassword = faker.internet.password()
export const invalidEmail = `${faker.name.firstName()}@${faker.name.lastName()}`